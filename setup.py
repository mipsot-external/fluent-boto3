from setuptools import find_packages, setup

setup(
    name='fluent-boto3',
    version='0.1.0.1',
    url='https://gitlab.com/mipsot-external/fluent-boto3',
    author='https://gitlab.com/mipsot',
    author_email='mipsot@ya.ru',
    license='MIT',
    packages=['fluent_boto3'],
    package_dir={'fluent_boto3': 'src'},
    install_requires=[
        'boto3',
        'wrapt',
    ],
    extras_require={
        'pretty-format': [
            'mydict==1.0.20'  # until https://github.com/leon-domingo/mydict/pull/7 was merged
        ]
    },
    description='Fluent compatible version of boto3.'
)

