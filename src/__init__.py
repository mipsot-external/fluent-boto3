""" Fluent compatible version of boto3.

Usage:
```
print(
    fluent_boto3.Stacks()['<StackName>'] \
        .resources['<EC2InstanceResourceLogicalId>']\
        .physically.public_host_address
)

print(
    fluent_boto3.Stacks()['<StackName>'] \
        .resources['<ALBResourceLogicalId>']\
        .physically.DNSName
)
```
"""

from contextlib import redirect_stdout, suppress
from functools import cached_property
from itertools import product
import re
import sys
from traceback import print_exc

import boto3
from boto3.exceptions import ResourceNotExistsError
from botocore.exceptions import ParamValidationError, UnknownServiceError
from botocore.utils import EVENT_ALIASES
from wrapt import ObjectProxy  # TODO: try to replace by collections.UserDict

with suppress(ModuleNotFoundError):
    from mydict import MyDict

Service2ResourceMapping = {}
for resource_alias, event_alias in EVENT_ALIASES.items():
    service_name = event_alias.replace('-', '')
    if service_name in Service2ResourceMapping:
        Service2ResourceMapping[service_name].append(resource_alias)
    else:
        Service2ResourceMapping[service_name] = [resource_alias]
RE_CAMEL_CASE = re.compile(r'(?<!^)(?=[A-Z])')
RE_PARAM_MUST_BE_ONE_OF_KWARGS = re.compile(
    r'(?<!must be one of:)\smust be one of:\s*'
    r'(([A-Za-z0-9_]+, [A-Za-z0-9_]+)+)$'
)
RE_PARAM_INVALID_TYPE = re.compile(r'Invalid type for parameter ')
SHOW_DECORATED_EXCEPTIONS = True


def print_exception(fn):
    def wrapper(*args, **kwargs):
        try:
            result = fn(*args, **kwargs)
        except BaseException:
            if SHOW_DECORATED_EXCEPTIONS:
                with redirect_stdout(sys.stderr):
                    print_exc()
            raise
        return result
    return wrapper


class SessionMixin():
    def __init__(self, *args, session=boto3.session.Session(), **kwargs):
        super().__init__(*args, **kwargs)
        self.session = session


class LazyCollection2DictMixin():
    # TODO: forbid mutations
    def __init__(self, *args, identifier_flt=lambda name: True, **kwargs):
        self.__wrapped__ = {}
        super().__init__(*args, **kwargs)
        self._fetched = False
        self._fetch_iter = iter(self)
        self.identifier_flt = identifier_flt

    def __getattribute__(self, name):
        return super().__getattribute__(name)

    def _get_item_key(self, item):
        item_key = tuple(
            getattr(item, name)
            for name in item.meta.identifiers
            if self.identifier_flt(name)
        )
        if len(item_key) == 1:
            [item_key] = item_key
        return item_key

    def __call__(self, item_id):
        for item_key in self._non_destructive_iter():
            item = self[item_key]
            if item_id == item_key:
                break
            res_model_name = item.meta.resource_model.name
            with suppress(TypeError, KeyError):
                if item_id == item[f'{res_model_name}Id']:
                    break
            attr_name = f'{RE_CAMEL_CASE.sub("_", res_model_name).lower()}_id'
            with suppress(AttributeError):
                if item_id == getattr(item, attr_name):
                    break
        else:
            return None
        return item

    def _fetch_up_to_element(self, item):
        while item not in self.__wrapped__ and not self._fetched:
            next(self._fetch_iter)

    def __getitem__(self, item):
        try:
            self._fetch_up_to_element(item)
        except StopIteration:
            raise KeyError(item)
        return self.__wrapped__[item]

    def __contains__(self, item):
        with suppress(StopIteration):
            self._fetch_up_to_element(item)
        return item in self.__wrapped__

    def __iter__(self):
        fetched = list(self.__wrapped__)
        yield from fetched
        fetched_counter = len(fetched)
        if self._fetched:
            return
        for item in self.collection.iterator():
            assert item
            if fetched_counter != len(self.__wrapped__):
                raise RuntimeError(
                    'dictionary changed size during iteration:'
                    f' {fetched_counter} -> {len(self.__wrapped__)}'
                )
            item_key = self._get_item_key(item)
            self.__wrapped__[item_key] = item
            fetched_counter += 1
            yield item_key
        self._fetched = True

    def _non_destructive_iter(self):
        i = 0
        while True:
            if len(self.__wrapped__) <= i:
                next(self._fetch_iter)
            yield list(self.__wrapped__)[i]
            i += 1

    def __len__(self):
        return len(list(self._fetch_iter))

    def __reversed__():
        return iter(reversed(list(self)))

    def __deepcopy__(self, memo):
        raise

    def copy(self):
        while not self._fetched:
            next(self._fetch_iter)
        return self.__wrapped__.copy()

    def get(self, key, default=None):
        if key in self:
            return self[key]
        return default

    def keys(self):  # TODO: use views https://docs.python.org/3/library/stdtypes.html#dict-views
        # TODO:
        #   self.keys calls ['__repr__', '__len__', '__iter__', '__len__', '__iter']
        return self._non_destructive_iter()

    def values(self):
        for key in self.keys():
            yield self[key]

    def items(self):
        for key in self.keys():
            yield (key, self[key])

    def __str__(self):
        list(self)
        return str(super())

    def __repr__(self):
        list(self)
        return repr(super())


class CollectionItemWrapper():
    def __init__(self, *args, item_wrapper=lambda self, obj: obj, **kwargs):
        super().__init__(*args, **kwargs)
        self.item_wrapper = item_wrapper

    def all(self):
        for item in self.__wrapped__.all():
            yield self.item_wrapper(item, session=self.session)

    def filter(self, **kwargs):
        for item in self.__wrapped__.filter(**kwargs):
            yield self.item_wrapper(item, session=self.session)

    def iterator(self, **kwargs):
        for item in self.__wrapped__.iterator(**kwargs):
            yield self.item_wrapper(item, session=self.session)

    def limit(self):
        raise NotImplementedError()

    def page_size(self, count):
        raise NotImplementedError()

    def pages(self):
        for page in self.__wrapped__.pages():
            yield [
                self.item_wrapper(item, session=self.session)
                for item in page
            ]

class CollectionFilterWrapper():
    def __init__(self, *args, item_filter=None, **kwargs):
        super().__init__(*args, **kwargs)
        self.item_filter = item_filter

    def all(self):
        for item in self.__wrapped__.all():
            if self.item_filter and self.item_filter(item):
                yield item

    def filter(self, **kwargs):
        if self.item_filter:
            raise RuntimeError("Not supported two filters at once")
        for item in self.__wrapped__.filter(**kwargs):
                yield item

    def iterator(self):
        for item in self.__wrapped__.iterator(**kwargs):
            if self.item_filter and self.item_filter(item):
                yield item

    def limit(self):
        raise NotImplementedError()

    def page_size(self):
        raise NotImplementedError()

    def pages(self):
        raise NotImplementedError()


class CollectionWrapper(SessionMixin,
                        CollectionItemWrapper,
                        CollectionFilterWrapper,
                        ObjectProxy):
    def __repr__(self):
        return f'fluent_boto3.CollectionWrapper({self.__wrapped__!r})'


class InstanceWrapper(SessionMixin, ObjectProxy):
    def __repr__(self):
        return f'fluent_boto3.InstanceWrapper({self.__wrapped__!r})'

    @property
    @print_exception
    def private_host_address(self):
        return self.private_dns_name \
            or self.private_ip_address

    @property
    @print_exception
    def public_host_address(self):
        return self.public_dns_name \
            or self.public_ip_address


class ResourceWrapper(SessionMixin, ObjectProxy):
    def __repr__(self):
        return f'fluent_boto3.ResourceWrapper({self.__wrapped__!r})'

    @cached_property
    def physically(self):
        return self.physical_resource

    @property
    @print_exception
    def physical_resource(self):
        service_provider, service_name, data_type_name = \
            self.resource_type.split('::')
        assert service_provider == 'AWS'
        resource_names = [service_name.lower()]
        if service_name.lower() in Service2ResourceMapping:
            resource_names = Service2ResourceMapping[service_name.lower()]
        attempts = [
            dict(
                constructor=self.session.resource,
                construct_loader_name=lambda name: name
            ),
            dict(
                constructor=self.session.client,
                construct_loader_name=lambda name: \
                    f'describe_{RE_CAMEL_CASE.sub("_", name).lower()}s'
            )
        ]
        for resource_name, attempt in product(resource_names, attempts):
            try:
                constructed = attempt['constructor'](resource_name)
            except (ResourceNotExistsError, UnknownServiceError):
                continue
            loader_name = attempt['construct_loader_name'](data_type_name)
            try:
                loader = getattr(constructed, loader_name)
            except AttributeError:
                continue
            break
        else:
            raise UnknownServiceError(', '.join(resource_names))
        physical_resource, loader_kwarg_names = None, [None, 'UndefinedKwarg']
        for loader_kwarg_name in loader_kwarg_names:
            try:
                args, kwargs = [], {}
                if loader_kwarg_name:
                    kwargs[loader_kwarg_name] = [self.physical_resource_id]
                else:
                    args.append(self.physical_resource_id)
                physical_resource = loader(*args, **kwargs)
                if loader_kwarg_name:
                    [physical_resource] = \
                        physical_resource[f'{data_type_name}s']
                    if 'MyDict' in globals():
                        physical_resource = MyDict(physical_resource)
            except TypeError as er:
                [text] = er.args
                if 'only accepts keyword arguments' not in text:
                    raise
            except ParamValidationError as er:
                text = er.kwargs['report']
                if RE_PARAM_INVALID_TYPE.match(text):
                    continue
                if not (search := RE_PARAM_MUST_BE_ONE_OF_KWARGS.search(text)):
                    raise
                for kwarg_name in search.group(1).split(', '):
                    if kwarg_name not in loader_kwarg_names:
                        loader_kwarg_names.append(kwarg_name)
            else:
                break
        else:
            raise TypeError('Cannot find physical_resource loader.')
        variables = globals()
        wrapper_class_name = f'{loader_name}Wrapper'
        if wrapper_class_name in variables:
            wrapper = variables[wrapper_class_name]
            return wrapper(physical_resource, session=self.session)
        return physical_resource


class ResourcesWrapper(SessionMixin, LazyCollection2DictMixin, object):
    def __repr__(self):
        return f'fluent_boto3.ResourcesWrapper({self.__wrapped__!r})'

    def __init__(self, collection, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._collection = collection

    @cached_property
    def collection(self):
        return CollectionWrapper(
            self._collection,
            session=self.session,
            item_wrapper=ResourceWrapper
        )


class StackWrapper(SessionMixin, ObjectProxy):
    def __repr__(self):
        return f'fluent_boto3.StackWrapper({self.__wrapped__!r})'

    @property
    @print_exception
    def params(self):
        result = {
            param['ParameterKey']: param['ParameterValue']
            for param in self.__wrapped__.parameters
        }
        if 'MyDict' in globals():
            result = MyDict(result)
        return result

    @property
    @print_exception
    def outs(self):
        result = {
            out['OutputKey']: out['OutputValue']
            for out in self.__wrapped__.outputs
        }
        if 'MyDict' in globals():
            result = MyDict(result)
        return result

    @property
    @print_exception
    def resources(self):
        return ResourcesWrapper(
            collection=self.resource_summaries,
            session=self.session,
            identifier_flt=lambda name: name != 'stack_name'
        )

    @print_exception
    def nested(self, recursively=False):
        for resource in self.resources:
            if isinstance(resource, str):  # FIXME:
                resource = self.resources[resource]
            if resource.resource_type == 'AWS::CloudFormation::Stack':
                stack = resource.physically
                yield stack
                if recursively:
                    yield from stack.nested(recursively=True)


class Stacks(SessionMixin, LazyCollection2DictMixin, object):
    def __init__(self, *args, with_nested=True, **kwargs):
        self.with_nested = with_nested
        super().__init__(*args, **kwargs)

    @cached_property
    def collection(self):
        item_filter = lambda stack: self.with_nested or stack.parent_id is None
        return CollectionWrapper(
            self.session.resource('cloudformation').stacks,
            session=self.session,
            item_wrapper=StackWrapper,
            item_filter=item_filter,
        )


class Instances(SessionMixin, LazyCollection2DictMixin, object):
    @cached_property
    def collection(self):
        return CollectionWrapper(
            self.session.resource('ec2').instances,
            session=self.session,
            item_wrapper=InstanceWrapper
        )
