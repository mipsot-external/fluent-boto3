# Description
Fluent compatible version of boto3.
# Installing
```bash
pip3 install git+https://gitlab.com/mipsot-external/fluent-boto3.git#egg=fluent_boto3[pretty-format]
```
# Usage
```python
print(
    fluent_boto3.Stacks()['<StackName>'] \
        .resources['<EC2InstanceResourceLogicalId>'] \
        .physically.public_host_address
)

print(
    fluent_boto3.Stacks()['<StackName>'] \
        .resources['<ALBResourceLogicalId>'] \
        .physically.DNSName
)
```
Classic syntax:
```python
In [1]: import boto3

In [2]: stack = boto3.resource('cloudformation').Stack('<StackName>')

In [3]: for resource in stack.resource_summaries.iterator():
   ...:     if resource.logical_id == '<EC2Stack>':
   ...:         break
   ...:

In [4]: resource.resource_type
Out[4]: 'AWS::CloudFormation::Stack'

In [5]: stack = boto3.resource('cloudformation').Stack(resource.physical_resource_id)

In [6]: for resource in stack.resource_summaries.iterator():
   ...:     if resource.logical_id == '<EC2Instance>':
   ...:         break
   ...: else:
   ...:     raise KeyError()
   ...:

In [7]: resource.resource_type
Out[7]: 'AWS::EC2::Instance'

In [8]: instance = boto3.resource('ec2').Instance(resource.physical_resource_id)

In [9]: instance.private_dns_name or instance.private_ip_address
Out[9]: 'ip-192-168-xxx-xxx.us-east-1.compute.internal'
```
Proposed syntax:
```python
In [1]: from fluent_boto3 import Stacks

In [2]: Stacks()['<StackName>'].resources['<EC2Stack>'].physically \
   ...:     .resources['<EC2Instance>'].physically.private_host_address
Out[2]: 'ip-192-168-xxx-xxx.us-east-1.compute.internal'
```
For stack arns:
```python
In [1]: from fluent_boto3 import Stacks

In [2]: stacks('arn:aws:cloudformation:<Region>:<AccountId>:stack/<EC2Stack>/<UUID>')
Out[2]: fluent_boto3.StackWrapper(cloudformation.Stack(name='<EC2Stack>'))
```

